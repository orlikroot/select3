package ua.dp.selector.service;

import ua.dp.selector.model.Category;

import java.util.List;

/**
 * Created by ekrot on 20.07.2016.
 */
public interface ProductService {
    List<Category> productList(String id);
}
