package ua.dp.selector.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ua.dp.selector.model.Category;
import ua.dp.selector.resources.FirstSubCategory;
import ua.dp.selector.resources.MainCategory;
import ua.dp.selector.resources.SecondSubCategory;
import ua.dp.selector.resources.ThirdSubCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ekrot on 20.07.2016.
 */
@Service
public class ProductServiceImpl implements ProductService {

    Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Override
    public List<Category> productList(String id) {

        logger.info("Call method productList with id = " + id);
        List<Category> list = new ArrayList<>();
        if (id.equals("none")) {
            for (MainCategory mainCategory : MainCategory.values()) {
                list.add(new Category(mainCategory.ordinal(), mainCategory.toString()));


            }
        } else { switch (id){
            case "0": {
                for (FirstSubCategory category : FirstSubCategory.values()) {
                    list.add(new Category(category.ordinal(), category.toString()));
                }}
                break;
            case "1": {
                for (SecondSubCategory category : SecondSubCategory.values()) {
                    list.add(new Category(category.ordinal(), category.toString()));
                }}
                break;
            case "2": {
                for (ThirdSubCategory category : ThirdSubCategory.values()) {
                    list.add(new Category(category.ordinal(), category.toString()));
                }}
                break;

        }

        }
        return list;
    }
}
