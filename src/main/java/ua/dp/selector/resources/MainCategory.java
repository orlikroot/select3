package ua.dp.selector.resources;

/**
 * Created by ekrot on 20.07.2016.
 */
public enum MainCategory {
    PC,
    Notebook,
    Smartphone
}
