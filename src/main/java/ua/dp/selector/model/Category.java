package ua.dp.selector.model;

import org.springframework.stereotype.Repository;

/**
 * Created by ekrot on 20.07.2016.
 */
@Repository
public class Category {

    private int id;
    private String name;

    public Category() {

    }

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
