package ua.dp.selector.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ua.dp.selector.model.Category;
import ua.dp.selector.service.ProductService;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Евгений on 20.07.2016.
 */
@RestController
@RequestMapping("/select")
public class RESTController {

    Logger logger = LoggerFactory.getLogger(RESTController.class);

    @Autowired
    ProductService productService;

    @RequestMapping(method = RequestMethod.GET, produces={MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    List<Category> select   (@RequestParam("id") String id) {

        logger.info("Select element " + id);
        return productService.productList(id);
    }
}
