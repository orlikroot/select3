package ua.dp.selector.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Евгений on 20.07.2016.
 */
@Controller
@RequestMapping("/")
public class MainController {

    Logger logger = LoggerFactory.getLogger(MainController.class);

        @RequestMapping(method = RequestMethod.GET)
    public String main(){
            logger.info("someone go to the main page");
        return "index";
    }

}
