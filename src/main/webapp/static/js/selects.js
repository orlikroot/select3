$(document).ready(function () {


    var url = '/select';
    $.get(
        url,
        "id=" + 'none',
        function (result) {
            if (result.type == 'error') {
                alert('error');
                return(false);
            }
            else {
                var options = '<option value="none">- select the type -</option>';
                $(result).each(function() {
                    options += '<option value="' + $(this).attr('id') + '">' + $(this).attr('name') + '</option>';
                });
                $('#category_id').html(options);
                $('#category_id').attr('disabled', false);
            }
        },
        "json"
    );
    

	$('#category_id').change(function () {
		var category_id = $(this).val();
		if (category_id == 'none') {
			$('#id').html('<option>- select the brand -</option>');
			$('#id').attr('disabled', true);
			return(false);
		}
		$('#id').attr('disabled', true);
		$('#id').html('<option>select...</option>');
		
		var url = '/select';
        var id = $(this).val();
		
		$.get(
			url,
			"id=" + id,
			function (result) {
				if (result.type == 'error') {
					alert('error');
					return(false);
				}
				else {
					var options = '<option value="none">- select the brand -</option>';
					$(result).each(function() {
						options += '<option value="' + $(this).attr('id') + '">' + $(this).attr('name') + '</option>';
					});
					$('#id').html(options);
					$('#id').attr('disabled', false);
				}
			},
			"json"
		);
	});
});
