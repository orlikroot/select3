<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Test Task</title>
  <link rel="stylesheet" href="static/css/main.css" type="text/css"/>
  <script type="application/javascript" src="static/js/jquery.js"></script>
  <script type="application/javascript" src="static/js/selects.js"></script>
  <script type="text/javascript">

  </script>
</head>

<body>

<form action="#" method="get">
  <p>Type</p>
  <select name="category_id" id="category_id">

  </select>
  <p>Brand:</p>
  <select name="id" id="id" disabled="disabled">

  </select>
</form>
</body>
</html>