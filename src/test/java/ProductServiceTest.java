import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ua.dp.selector.model.Category;
import ua.dp.selector.service.ProductService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by ekrot on 20.07.2016.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:dispatcher-servlet.xml")
public class ProductServiceTest {

    @Autowired
ProductService productService;
//    ProductService productService = new ProductServiceImpl();

    @Test
    public void getTestFirstCategory(){
        String id = "0";

        Category testCategory = new Category();
        testCategory.setId(0);
        testCategory.setName("ASUS");
        assertEquals(productService.productList(id).get(0).getName(), testCategory.getName());
        assertEquals(productService.productList(id).get(0).getId(), testCategory.getId());
        assertNotEquals(productService.productList(id).get(1).getId(), testCategory.getName());
        assertNotEquals(productService.productList(id).get(1).getId(), testCategory.getId());

    }

    @Test
    public void getTestSecondCategory(){
        String id = "1";
        Category testCategory = new Category();
        testCategory.setId(0);
        testCategory.setName("HP");
        assertEquals(productService.productList(id).get(0).getName(), testCategory.getName());
        assertEquals(productService.productList(id).get(0).getId(), testCategory.getId());
        assertNotEquals(productService.productList(id).get(1).getId(), testCategory.getName());
        assertNotEquals(productService.productList(id).get(1).getId(), testCategory.getId());
    }

    @Test
    public void getTestThirdCategory(){
        String id = "2";
        Category testCategory = new Category();
        testCategory.setId(2);
        testCategory.setName("Xiaomi");
        assertEquals(productService.productList(id).get(2).getName(), testCategory.getName());
        assertEquals(productService.productList(id).get(2).getId(), testCategory.getId());
        assertNotEquals(productService.productList(id).get(1).getId(), testCategory.getName());
        assertNotEquals(productService.productList(id).get(1).getId(), testCategory.getId());
    }
}
